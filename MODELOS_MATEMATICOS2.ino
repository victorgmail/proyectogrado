
float modeloMatematico_MQ3_ALC(float entrada_MQ3_ALC) {
  salida_MQ3_ALC = pow(10,(((log(entrada_MQ3_ALC)-y_1_MQ3_ALC)/(m_MQ3_ALC))+x_1_MQ3_ALC));
  return salida_MQ3_ALC;
}

float modeloMatematico_MQ135_ACE(float entrada_MQ135_ACE) {
  salida_MQ135_ACE = pow(10,(((log(entrada_MQ135_ACE)-y_1_MQ135_ACE)/(m_MQ135_ACE))+x_1_MQ135_ACE));
  return salida_MQ135_ACE;
}

float modeloMatematico_MQ6_CH4(float entrada_MQ6_CH4) {
  salida_MQ6_CH4 = pow(10,(((log(entrada_MQ6_CH4)-y_1_MQ6_CH4)/(m_MQ6_CH4))+x_1_MQ6_CH4));
  return salida_MQ6_CH4;
}

float modeloMatematico_MQ5_LPG(float entrada_MQ5_LPG) {
  salida_MQ5_LPG = pow(10,(((log(entrada_MQ5_LPG)-y_1_MQ5_LPG)/(m_MQ5_LPG))+x_1_MQ5_LPG));
  return salida_MQ5_LPG;
}

float modeloMatematico_MQ3_BEN(float entrada_MQ3_BEN) {
salida_MQ3_BEN = pow(10,(((log(entrada_MQ3_BEN)-y_1_MQ3_BEN)/(m_MQ3_BEN))+x_1_MQ3_BEN));
return salida_MQ3_BEN;
}

float modeloMatematico_MQ135_CO2(float entrada_MQ135_CO2) {
salida_MQ135_CO2 = pow(10,(((log(entrada_MQ135_CO2)-y_1_MQ135_CO2)/(m_MQ135_CO2))+x_1_MQ135_CO2));
return salida_MQ135_CO2;
}

float modeloMatematico_MQ6_CO(float entrada_MQ6_CO) {
salida_MQ6_CO = pow(10,(((log(entrada_MQ6_CO)-y_1_MQ6_CO)/(m_MQ6_CO))+x_1_MQ6_CO));
return salida_MQ6_CO;
}

float modeloMatematico_MQ5_H2(float entrada_MQ5_H2) {
salida_MQ5_H2 = pow(10,(((log(entrada_MQ5_H2)-y_1_MQ5_H2)/(m_MQ5_H2))+x_1_MQ5_H2));
return salida_MQ5_H2;
}
