// TEMPERATURA 26 °C AMBIENTE CALCULADO POR DHT 11
// HUMEDAD DE 27%    AMBIENTE CALCULADO POR DHT 11
// RECORDAR QUE SI EL SENSOR ES NUEVO DEBE PRECALENTARSE DURANTE 24HORAS MINIMO

const float densidad_aire = 1.29; //en g/L
// DECLARACION VARIABLES MQ135 ACETONA---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const int MQ135_ACE_PIN = A0; //ACE=ACETONA
int RL_MQ135_ACE = 905; 
int RO_MQ135_ACE = 2573.17; //valor de Ro el cual es la resistencia que tiene el MQ-135 a 100ppm de NH3 en aire limpio
float MEDICION_ppm_MQ135_ACE; //variable donde se guarda la concentracion de acetona en ppm
float RAZON_MEDICION_MQ135_ACE;

// DECLARACION VARIABLES MQ6 METANO---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const int MQ6_CH4_PIN = A1; 
int RL_MQ6_CH4 = 982; 
int RO_MQ6_CH4 = 4037.62; //valor de Ro el cual es la resistencia que tiene el MQ-6 a 1000ppm de LPG en aire limpio
float MEDICION_ppm_MQ6_CH4;
float RAZON_MEDICION_MQ6_CH4;

// DECLARACION VARIABLES MQ3 ALCOHOL---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const int MQ3_ALC_PIN = A2; //conexion del sensor MQ con el arduino
int RL_MQ3_ALC = 991; // valor de RL (resistencia donde cae el voltaje que mide arduino)
int RO_MQ3_ALC = 3620.3; // valor de Ro el cual es la resistencia que tiene el MQ-3 a 0.4mg/L de alcohol en aire limpio
float MEDICION_mg_L_MQ3_ALC; //variable donde se guarda la concentracion de alcohol en mg/L
float MEDICION_ppm_MQ3_ALC; //variable donde se guarda la concentracion de alcohol en ppm
float RAZON_MEDICION_MQ3_ALC; //variable donde se guarda el valor de Rs/Ro

// DECLARACION VARIABLES MQ5 GAS LICUADO DE PETROLEO---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const int MQ5_LPG_PIN = A3;
int RL_MQ5_LPG = 968;
int RO_MQ5_LPG = 10234.32; //valor de Ro el cual es la resistencia que tiene el MQ-135 a 1000pm de H2 en aire limpio
float MEDICION_ppm_MQ5_LPG; 
float RAZON_MEDICION_MQ5_LPG;

// DECLARACION VARIABLES MQ3_BENCENO--------------------------------------
const int MQ3_BEN_PIN = A5;
int RL_MQ3_BEN = 950; 
int RO_MQ3_BEN = 3200; 
float MEDICION_mg_L_MQ3_BEN;
float MEDICION_ppm_MQ3_BEN;
float RAZON_MEDICION_MQ3_BEN;

// DECLARACION VARIABLES MQ135 DIOXIDO DE CARBONO--------------------------------------
const int MQ135_CO2_PIN = A7;
int RL_MQ135_CO2 = 985;
int RO_MQ135_CO2 = 3238.81; 
float MEDICION_ppm_MQ135_CO2;
float RAZON_MEDICION_MQ135_CO2;

// DECLARACION VARIABLES MQ6 MONOXIDO DE CARBONO--------------------------------------
const int MQ6_CO_PIN = A4;
int RL_MQ6_CO = 988;
int RO_MQ6_CO = 1640.71;
float MEDICION_ppm_MQ6_CO;
float RAZON_MEDICION_MQ6_CO;

// DECLARACION VARIABLES MQ5 DIHIDROGENO--------------------------------------
const int MQ5_H2_PIN = A6;
int RL_MQ5_H2 = 985;
int RO_MQ5_H2 = 2124.31;
float MEDICION_ppm_MQ5_H2;
float RAZON_MEDICION_MQ5_H2;

// DECLARACION VARIABLES MQ7-1 MONOXIDO DE CARBONO
const int MQ7_CO_PIN = A8;
int RL_MQ7_CO = 950;
int RO_MQ7_CO = 2125;
float MEDICION_ppm_MQ7_CO;
float RAZON_MEDICION_MQ7_CO;

// DECLARACION VARIABLES MQ7-2 MONOXIDO DE CARBONO
const int MQ72_CO_PIN = A9;
int RL_MQ72_CO = 950;
int RO_MQ72_CO = 2125;
float MEDICION_ppm_MQ72_CO;
float RAZON_MEDICION_MQ72_CO;


// CONSTANTES MODELO MATEMATICO MQ135 ACETONA------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const float y_1_MQ135_ACE=log(1.45);
const float x_1_MQ135_ACE=log(10);
const float m_MQ135_ACE=-0.3001591;
float salida_MQ135_ACE = 0;
// CONSTANTES MODELO MATEMATICO MQ6 METANO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const float y_1_MQ6_CH4=log(2.6);
const float x_1_MQ6_CH4=log(200);
const float m_MQ6_CH4=-0.39246445;
float salida_MQ6_CH4 = 0;
// CONSTANTES MODELO MATEMATICO MQ3 ALCOHOL------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const float y_1_MQ3_ALC=log(2.35); //Y1
const float x_1_MQ3_ALC=log(0.1); //X1
const float m_MQ3_ALC=-0.62856; //m
float salida_MQ3_ALC = 0; //variable que se devuelve en la funcion del modelo matematico
// CONSTANTES MODELO MATEMATICO MQ5 GAS LICUADO DE PETROLEO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
const float y_1_MQ5_LPG = log(0.7);
const float x_1_MQ5_LPG = log(200);
const float m_MQ5_LPG = -0.406884;
float salida_MQ5_LPG = 0;
// CONSTANTES MODELO MATEMATICO MQ6 MONOXIDO DE CARBORNO---------------------------------------------------
const float y_1_MQ6_CO = log(8.87);
const float x_1_MQ6_CO = log(200);
const float m_MQ6_CO = -0.07430759;
float salida_MQ6_CO = 0;
// CONSTANTES MODELO MATEMATICO MQ3 BENCENO-----------------------------------------
const float y_1_MQ3_BEN = log(4.2);
const float x_1_MQ3_BEN = log(0.1);
const float m_MQ3_BEN = -0.37996322;
float salida_MQ3_BEN = 0;
// CONSTANTES MODELO MATEMATICO MQ5 DIHIDROGENO ---------------------------------------------------
const float y_1_MQ5_H2 = log(1.75);
const float x_1_MQ5_H2 = log(200);
const float m_MQ5_H2 = -0.34335703;
float salida_MQ5_H2 = 0;
// CONSTANTES MODELO MATEMATICO MQ135 DIOXIDO DE CARBONO-----------------------------------------
const float y_1_MQ135_CO2 = log(2.3);
const float x_1_MQ135_CO2 = log(10);
const float m_MQ135_CO2 = -0.35251904;
float salida_MQ135_CO2 = 0;
// CONSTANTES MODELO MATEMATICO MQ7 DIHIDROGENO ---------------------------------------------------
const float y_1_MQ7_CO = log(1.7);
const float x_1_MQ7_CO = log(14);
const float m_MQ7_CO = -0.6485397;
float salida_MQ7_CO = 0;
// CONSTANTES MODELO MATEMATICO MQ7 monoxido DE CARBONO-----------------------------------------
const float y_1_MQ72_CO = log(1.7);
const float x_1_MQ72_CO = log(14);
const float m_MQ72_CO = -0.6485397;
float salida_MQ72_CO = 0;

//CONSTANTES FILTRADO DE DATOS--------------------------------------------------------------------------------------------------------------------------------------------
#define alpha_MQ135_ACE 0.1
#define alpha_MQ6_CH4 0.05
#define alpha_MQ3_ALC 0.1
#define alpha_MQ5_LPG 0.1
#define alpha_MQ6_CO 0.05
#define alpha_MQ3_BEN 0.1
#define alpha_MQ5_H2 0.1
#define alpha_MQ135_CO2 0.1
#define alpha_MQ7_CO 0.1
#define alpha_MQ72_CO 0.1
//salidas filtradas con media movil (filtro pasa bajo digital)--------------------------------------------------------------------------------------------------------------------------------------------------------
float MEDICION_ppm_MQ3_ALC_FILTRADO;
float MEDICION_ppm_MQ135_ACE_FILTRADO;
float MEDICION_ppm_MQ6_CH4_FILTRADO; 
float MEDICION_ppm_MQ5_LPG_FILTRADO;
float MEDICION_ppm_MQ3_BEN_FILTRADO;
float MEDICION_ppm_MQ135_CO2_FILTRADO;
float MEDICION_ppm_MQ6_CO_FILTRADO;
float MEDICION_ppm_MQ5_H2_FILTRADO;
float MEDICION_ppm_MQ7_CO_FILTRADO;
float MEDICION_ppm_MQ72_CO_FILTRADO;

void setup() {
  Serial.begin(9600);
//  Serial.println("MQ3alcohol\tMQ135acetona\tMQ6metano\tMQ5lpg\tMQ3benceno\tMQ135co2\tMQ6co\tMQ5h2"); // etiquetas para graficar con serial plotter (deben estar comentadas si esta activada la conexion a excel)
  Serial.println("LABEL,hora,ALCOHOL[PPM],ACETONA[PPM],METANO[PPM],LPG[PPM],BENCENO[PPM],CO2[PPM],CO[PPM],H2[PPM],CO[PPM],2-CO[PPM]"); // Conexion excel
}

void loop() {
// RAZONES DE MEDICION-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  RAZON_MEDICION_MQ3_ALC = (((1023-(float)analogRead(MQ3_ALC_PIN))/(float)analogRead(MQ3_ALC_PIN))*RL_MQ3_ALC)/RO_MQ3_ALC; //calculo de Rs/Ro usando divisor de voltaje y la constante Ro
  RAZON_MEDICION_MQ135_ACE = (((1023-(float)analogRead(MQ135_ACE_PIN))/(float)analogRead(MQ135_ACE_PIN))*RL_MQ135_ACE)/RO_MQ135_ACE;
  RAZON_MEDICION_MQ6_CH4 = (((1023-(float)analogRead(MQ6_CH4_PIN))/(float)analogRead(MQ6_CH4_PIN))*RL_MQ6_CH4)/RO_MQ6_CH4;
  RAZON_MEDICION_MQ5_LPG = (((1023-(float)analogRead(MQ5_LPG_PIN))/(float)analogRead(MQ5_LPG_PIN))*RL_MQ5_LPG)/RO_MQ5_LPG;
  RAZON_MEDICION_MQ3_BEN = (((1023-(float)analogRead(MQ3_BEN_PIN))/(float)analogRead(MQ3_BEN_PIN))*RL_MQ3_BEN)/RO_MQ3_BEN;
  RAZON_MEDICION_MQ135_CO2 = (((1023-(float)analogRead(MQ135_CO2_PIN))/(float)analogRead(MQ135_CO2_PIN))*RL_MQ135_CO2)/RO_MQ135_CO2;
  RAZON_MEDICION_MQ6_CO = (((1023-(float)analogRead(MQ6_CO_PIN))/(float)analogRead(MQ6_CO_PIN))*RL_MQ6_CO)/RO_MQ6_CO;
  RAZON_MEDICION_MQ5_H2 = (((1023-(float)analogRead(MQ5_H2_PIN))/(float)analogRead(MQ5_H2_PIN))*RL_MQ5_H2)/RO_MQ5_H2;
  RAZON_MEDICION_MQ7_CO = (((1023-(float)analogRead(MQ7_CO_PIN))/(float)analogRead(MQ7_CO_PIN))*RL_MQ7_CO)/RO_MQ7_CO;
  RAZON_MEDICION_MQ72_CO = (((1023-(float)analogRead(MQ72_CO_PIN))/(float)analogRead(MQ72_CO_PIN))*RL_MQ72_CO)/RO_MQ72_CO;
// MEDICION CONCENTRACION CON MODELOS MATEMATICOS--------------------------------------------------------------------------------------------------------------------------------------------------------
  MEDICION_mg_L_MQ3_ALC = modeloMatematico_MQ3_ALC(RAZON_MEDICION_MQ3_ALC); // calculo de las concentraciones, se recuerda que los modelos matematicos estan en funcion de Ro/Rs
  MEDICION_ppm_MQ135_ACE = modeloMatematico_MQ135_ACE(RAZON_MEDICION_MQ135_ACE);//para ver los modelos matematicos irse al fichero "modelos_matematicos"
  MEDICION_ppm_MQ6_CH4 = modeloMatematico_MQ6_CH4(RAZON_MEDICION_MQ6_CH4);
  MEDICION_ppm_MQ5_LPG = modeloMatematico_MQ5_LPG(RAZON_MEDICION_MQ5_LPG);
  MEDICION_mg_L_MQ3_BEN = modeloMatematico_MQ3_BEN(RAZON_MEDICION_MQ3_BEN);
  MEDICION_ppm_MQ135_CO2 = modeloMatematico_MQ135_CO2(RAZON_MEDICION_MQ135_CO2);
  MEDICION_ppm_MQ6_CO = modeloMatematico_MQ6_CO(RAZON_MEDICION_MQ6_CO);
  MEDICION_ppm_MQ5_H2 = modeloMatematico_MQ5_H2(RAZON_MEDICION_MQ5_H2);
  MEDICION_ppm_MQ7_CO = modeloMatematico_MQ7_CO(RAZON_MEDICION_MQ7_CO);
  MEDICION_ppm_MQ72_CO = modeloMatematico_MQ72_CO(RAZON_MEDICION_MQ72_CO);
// MEDICION CONCENTRACION CON MODELOS MATEMATICOS--------------------------------------------------------------------------------------------------------------------------------------------------------
  recortadores(); //cualquier valor que este fuera del campo de medida del sensor se vuelve cero ya que fuera del rango las medidas no son exactas
// CONVERSOR MEDICIONES DE MQ3 A PPM-------------------------------------------------------------------------------------------------------------------------------------
  MEDICION_ppm_MQ3_ALC = ((MEDICION_mg_L_MQ3_ALC)*1000)/densidad_aire; //los modelos matematicos del MQ3 devuelven la concentracion en mg/L. para volverlos a ppm
  MEDICION_ppm_MQ3_BEN = ((MEDICION_mg_L_MQ3_BEN)*1000)/densidad_aire; //se dividen entre la densidad de aire en g/L y se multiplica por 1000
// MEDIDAS FILTRADAS-------------------------------------------------------------------------------------------------------------------------------------
  MEDICION_ppm_MQ3_ALC_FILTRADO=(alpha_MQ3_ALC*MEDICION_ppm_MQ3_ALC)+((1-alpha_MQ3_ALC)*MEDICION_ppm_MQ3_ALC_FILTRADO); //se aplica la formula del filtro pasa bajo
  MEDICION_ppm_MQ135_ACE_FILTRADO=(alpha_MQ135_ACE*MEDICION_ppm_MQ135_ACE)+((1-alpha_MQ135_ACE)*MEDICION_ppm_MQ135_ACE_FILTRADO);
  MEDICION_ppm_MQ6_CH4_FILTRADO=(alpha_MQ6_CH4*MEDICION_ppm_MQ6_CH4)+((1-alpha_MQ6_CH4)*MEDICION_ppm_MQ6_CH4_FILTRADO);
  MEDICION_ppm_MQ5_LPG_FILTRADO=(alpha_MQ5_LPG*MEDICION_ppm_MQ5_LPG)+((1-alpha_MQ5_LPG)*MEDICION_ppm_MQ5_LPG_FILTRADO);
  MEDICION_ppm_MQ3_BEN_FILTRADO=(alpha_MQ3_BEN*MEDICION_ppm_MQ3_BEN)+((1-alpha_MQ3_BEN)*MEDICION_ppm_MQ3_BEN_FILTRADO);
  MEDICION_ppm_MQ135_CO2_FILTRADO=(alpha_MQ135_CO2*MEDICION_ppm_MQ135_CO2)+((1-alpha_MQ135_CO2)*MEDICION_ppm_MQ135_CO2_FILTRADO);
  MEDICION_ppm_MQ6_CO_FILTRADO=(alpha_MQ6_CO*MEDICION_ppm_MQ6_CO)+((1-alpha_MQ6_CO)*MEDICION_ppm_MQ6_CO_FILTRADO);
  MEDICION_ppm_MQ5_H2_FILTRADO=(alpha_MQ5_H2*MEDICION_ppm_MQ5_H2)+((1-alpha_MQ5_H2)*MEDICION_ppm_MQ5_H2_FILTRADO);
  MEDICION_ppm_MQ7_CO_FILTRADO=(alpha_MQ7_CO*MEDICION_ppm_MQ7_CO)+((1-alpha_MQ7_CO)*MEDICION_ppm_MQ7_CO_FILTRADO);
  MEDICION_ppm_MQ72_CO_FILTRADO=(alpha_MQ72_CO*MEDICION_ppm_MQ72_CO)+((1-alpha_MQ72_CO)*MEDICION_ppm_MQ72_CO_FILTRADO);
// IMPRESION DE LOS DATOS--------------------------------------------------------------------------------------------------------------------------------------------------------
  Serial.print("DATA,TIME,");//se imprimen los datos a excel
  Serial.print(MEDICION_ppm_MQ3_ALC_FILTRADO);
  Serial.print(",");
  Serial.print(MEDICION_ppm_MQ135_ACE_FILTRADO);
  Serial.print(",");
  Serial.print(MEDICION_ppm_MQ6_CH4_FILTRADO);
  Serial.print(",");
  Serial.print(MEDICION_ppm_MQ5_LPG_FILTRADO);
  Serial.print(",");
  Serial.print(MEDICION_ppm_MQ3_BEN_FILTRADO);
  Serial.print(",");
  Serial.print(MEDICION_ppm_MQ135_CO2_FILTRADO);
  Serial.print(",");
  Serial.print(MEDICION_ppm_MQ6_CO_FILTRADO);
  Serial.print(",");
  Serial.println(MEDICION_ppm_MQ5_H2_FILTRADO);
  Serial.print(",");
  Serial.print(MEDICION_ppm_MQ7_CO_FILTRADO);
  Serial.print(",");
  Serial.println(MEDICION_ppm_MQ72_CO_FILTRADO);
  delay(100);
}

void recortadores(){
//RECORTADOR MQ3 ALCOHOL------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_mg_L_MQ3_ALC < 0.1 || MEDICION_mg_L_MQ3_ALC > 10){
    MEDICION_mg_L_MQ3_ALC = 0;
  }
//RECORTADOR MQ135 ACETONA------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_ppm_MQ135_ACE < 10 || MEDICION_ppm_MQ135_ACE > 10000){
    MEDICION_ppm_MQ135_ACE = 0;
  }
//RECORTADOR MQ6 METANO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_ppm_MQ6_CH4 < 200 || MEDICION_ppm_MQ6_CH4 > 10000){
    MEDICION_ppm_MQ6_CH4 = 0;
  }
//RECORTADOR MQ5 GAS LICUADO DE PETROLEO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_ppm_MQ5_LPG < 200 || MEDICION_ppm_MQ5_LPG > 10000){
    MEDICION_ppm_MQ5_LPG = 0;
  }
//RECORTADOR MQ3 BENCENO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_mg_L_MQ3_BEN < 0.1 || MEDICION_mg_L_MQ3_BEN > 10){
    MEDICION_mg_L_MQ3_BEN = 0;
  }
//RECORTADOR MQ135 DIOXIDO DE CARBONO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_ppm_MQ135_CO2 < 10|| MEDICION_ppm_MQ135_CO2 > 1000){
    MEDICION_ppm_MQ135_CO2 = 0;
  }
//RECORTADOR MQ6 MONOXIDO DE CARBONO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_ppm_MQ6_CO < 200|| MEDICION_ppm_MQ6_CO > 10000){
    MEDICION_ppm_MQ6_CO = 0;
  }
//RECORTADOR MQ5 DIHIDROGENO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_ppm_MQ5_H2 < 200|| MEDICION_ppm_MQ5_H2> 3000){
    MEDICION_ppm_MQ5_H2 = 0;
  }
  //RECORTADOR MQ7 MONOXIDO DE CARBONO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_ppm_MQ7_CO < 200|| MEDICION_ppm_MQ7_CO > 10000){
    MEDICION_ppm_MQ7_CO = 0;
  }
//RECORTADOR MQ7 DIHIDROGENO------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (MEDICION_ppm_MQ72_CO < 200|| MEDICION_ppm_MQ72_CO> 3000){
    MEDICION_ppm_MQ72_CO = 0;
  }
}

float modeloMatematico_MQ7_CO(float entrada_MQ7_CO) {
salida_MQ7_CO = pow(10,(((log(entrada_MQ7_CO)-y_1_MQ7_CO)/(m_MQ7_CO))+x_1_MQ7_CO));
return salida_MQ7_CO;
}

float modeloMatematico_MQ72_CO(float entrada_MQ72_CO) {
salida_MQ72_CO = pow(10,(((log(entrada_MQ72_CO)-y_1_MQ72_CO)/(m_MQ72_CO))+x_1_MQ72_CO));
return salida_MQ72_CO;
}
